from django.apps import AppConfig


class WelcomeConfig(AppConfig):
    name = 'fwaNew.apps.welcome'
